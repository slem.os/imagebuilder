#!/bin/bash
set -e

source .env

if [[ $QUESTIONS_AUTO_ACCEPT =~ ^[Yy]$ ]]
then
    echo ""
else
  echo "This script will download a image of openSUSE from official site"
  echo
  read -p "Continue? [y/N] " -n 1 -r
  echo
  if [[ $REPLY =~ ^[Nn]$ ]] || [[ -z $REPLY ]]
  then
      exit 0
  fi
fi

# Download kernel
#wget https://xff.cz/kernels/5.7/pp2.tar.gz -O pp.tar.gz
#tar xf pp.tar.gz

# Get latest rawhide from repo when not set in .env
if [ -z "$SUSE_RAW_FILE" ]
then
    echo "Searching for latest Fedora Rawhide..."
    SUSE_RAW_VER=$(wget -q $SUSE_RAW_SOURCE -O - | grep -Po '(?<=openSUSE-Tumbleweed-ARM-JeOS-raspberrypi4.aarch64-).*?(?=.raw.xz)' | head -n 1)
    if [ ! -z "$SUSE_RAW_VER" ]
    then
        echo "Downloading latest openSUSE version: $SUSE_RAW_VER"
        SUSE_RAW_FILE=openSUSE-Tumbleweed-ARM-JeOS-raspberrypi4.aarch64-${SUSE_RAW_VER}.raw.xz
        echo ""
    else
        echo "Could not obtain latest version data"
        echo "Please visit $SUSE_RAW_SOURCE uncomment and update the SUSE_RAW_FILE name in .env with the name on that website."
        exit 1
    fi
else
    echo "Downloading SUSE set in .env file: $SUSE_RAW_FILE"
    echo "This may fail if it is set to download a file that is too old."
    echo ""
fi

# Download fedora
wget $SUSE_RAW_SOURCE/$SUSE_RAW_FILE -O rawhide.raw.xz
xz --decompress rawhide.raw.xz
