#!/bin/bash

source .env
NIGHTLY_NAME=(${OUT_NAME%.img}-$(date +%Y%m%d).img)
mv $OUT_NAME $NIGHTLY_NAME
