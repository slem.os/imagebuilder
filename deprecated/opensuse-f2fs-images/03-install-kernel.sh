#!/bin/bash
set -e

source .env

echo "===================="
echo "03-install-kernel.sh"
echo "===================="

# Functions
infecho () {
    echo "[Info] $1"
}

# Notify User
infecho "The env vars that will be used in this script..."
infecho "PP_SD_DEVICE = $PP_IMAGE"
infecho "PP_PARTA = $PP_PARTA"
infecho "PP_PARTB = $PP_PARTB"
echo

# Automatic Preflight Checks
if [[ $EUID -ne 0 ]]; then
    errecho "This script must be run as root!"
    exit 1
fi

# Warning
if [[ $QUESTIONS_AUTO_ACCEPT =~ ^[Yy]$ ]]
then
    echo ""
else
    echo "=== WARNING WARNING WARNING ==="
    infecho "This script USES THE DD COMMAND AS ROOT. If the env vars are wrong, this could do something bad."
    infecho "Make sure this script is run from the main dir of the repo, since it assumes that's true."
    infecho "Also, I didn't test this so it might also cause WWIII or something."
    infecho "I'm not responsible for anything that happens, you should read the script first."
    echo "=== WARNING WARNING WARNING ==="
    echo
    read -p "Continue? [y/N] " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Nn]$ ]] || [[ -z $REPLY ]]
    then
        exit 0
    fi
fi

infecho "Changing directory..."
cd $KERNEL_RAW_DIR

# infecho "Generating boot.scr..."
# mkimage -A arm64 -T script -C none -d boot.cmd boot.scr

infecho "Writing bootloader..."
dd if=uboot.bin of=$PP_IMAGE bs=1024 seek=8

infecho "Changing directory..."
cd ../

infecho "Mounting SD card partitions..."
mkdir -p bootfs
mkdir -p rootfs
mount $PP_PARTA bootfs
mount $PP_PARTB rootfs

infecho "Copying boot.scr board.itb..."
mkdir -p bootfs/dtb/
cp $KERNEL_RAW_DIR/boot.scr bootfs/
cp $KERNEL_RAW_DIR/*.dtb bootfs/dtb
cp $KERNEL_RAW_DIR/*.dts* bootfs/dtb
cp $KERNEL_RAW_DIR/Image bootfs/
#cp $KERNEL_RAW_DIR/board.itb bootfs/

infecho "Installing kernel modules..."
rsync -a --progress $KERNEL_RAW_DIR/modules/lib/modules/* rootfs/lib/modules/

infecho "Unmounting SD card partitions..."
umount $PP_PARTA
umount $PP_PARTB
rmdir bootfs
rmdir rootfs
