#!/bin/bash
set -e

source .env

echo "========================"
echo "00-create-empty-image.sh"
echo "========================"

if [[ $QUESTIONS_AUTO_ACCEPT =~ ^[Yy]$ ]]
then
    echo ""
else
    echo "Create image called $OUT_NAME? Make sure this doesn't exist, or it will be zeroed out."
    read -p "Continue? [y/N] " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Nn]$ ]] || [[ -z $REPLY ]]
    then
        exit 0
    fi
fi

# Approximately 4GB image.
echo "Creating blank 4GB file called $OUT_NAME."
dd if=/dev/zero of=$OUT_NAME iflag=fullblock bs=1M count=4000 && sync
