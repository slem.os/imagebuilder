#!/bin/bash
set -e

source .env

echo "=============="
echo "debug-mount.sh"
echo "=============="

# Functions
infecho () {
    echo "[Info] $1"
}
errecho () {
    echo $1 1>&2
}

# Automatic Preflight Checks
if [[ $EUID -ne 0 ]]; then
    errecho "This script must be run as root!"
    exit 1
fi

mkdir -p rootfs
losetup /dev/loop1 $OUT_NAME
mount /dev/loop1p2 rootfs/
mount --bind /dev rootfs/dev
infecho "Execute: sudo chroot rootfs qemu-aarch64-static /bin/bash"
