#!/bin/bash
set -e

bash /root/installation/01-create-sudo-user.sh
bash /root/installation/02-install-packages.sh
bash /root/installation/03-configure-system.sh
