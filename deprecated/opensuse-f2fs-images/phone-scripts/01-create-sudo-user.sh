#!/bin/bash
set -e

echo "======================"
echo "01-create-sudo-user.sh"
echo "======================"

# Functions
infecho () {
    echo "[Info] $1"
}

infecho "Adding user \"pine\"..."
useradd -m pine
echo -e "1234\n1234" | (passwd pine)

infecho "Create and add user pine to correct groups"
groupadd plugdev
groupadd netdev
usermod -a -G plugdev pine
usermod -a -G audio pine
usermod -a -G video pine
usermod -a -G dialout pine
usermod -a -G render pine
usermod -a -G input pine
usermod -a -G netdev pine

infecho "Add user \"pine\" to sudoers file"
echo "pine  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/pine
