#!/bin/bash
set -e

echo "==================="
echo "03-configure-system.sh"
echo "==================="

# Functions
infecho () {
    echo "[Info] $1"
}
errecho () {
    echo "[Error] $1" 1>&2
    exit 1
}

infecho "Configure hostname"
echo "opensuse-pinephone" > /etc/hostname

infecho "Configure background"
mkdir /usr/share/wallpapers
cp  /root/installation/opensuse-background.jpg /usr/share/wallpapers/
chmod 777 -R /usr/share/wallpapers/
sudo -u pine gsettings set org.gnome.desktop.background picture-uri "file:///usr/share/wallpapers/opensuse-background.jpg"
#sed -i "s/openSUSE-default.xml/opensuse-background.jpg/g" /usr/share/glib-2.0/schemas/openSUSE-branding.gschema.override

infecho "Configure GNOME Control Center Logo"
mkdir -p /usr/share/icons/vendor/scalable/emblems/
cp /root/installation/square-hicolor.svg /usr/share/icons/vendor/scalable/emblems/emblem-vendor.svg

infecho "Configure Glib Schemas"
cp /root/installation/90_pinephone.gschema.override /usr/share/glib-2.0/schemas/90_pinephone.gschema.override

infecho "Configure Sound"
mkdir /usr/share/alsa/ucm2/PinePhone/
cp /root/installation/alsa-ucm/* /usr/share/alsa/ucm2/PinePhone/
mkdir -p /home/pine/.config/systemd/user/default.target.wants/
ln -s /usr/lib/systemd/user/wys.service /home/pine/.config/systemd/user/default.target.wants/wys.service
chown pine:users -R /home/pine/.config/


infecho "Configure polkit rules"
cp /root/installation/90-modem-manager.rules /usr/share/polkit-1/rules.d/
cp /root/installation/91-network-manager.rules /usr/share/polkit-1/rules.d/

#infecho "Configure Volume Keys"
#cp /root/installation/actkbd/actkbd_env /etc/sysconfig/actkbd
#cp /root/installation/actkbd/actkbd.conf /etc/actkbd.conf
#sed -i '/^ExecStart=.*/a User=pine' /usr/lib/systemd/system/actkbd.service
#systemctl enable actkbd

infecho "Deleting unused desktop icons"
rm -f /usr/share/applications/YaST2/org.opensuse.yast.CheckMedia.desktop
rm -f /usr/share/applications/YaST2/messages.desktop

infecho "Configure udev rules"
cp /root/installation/udev-rules/* /etc/udev/rules.d/

infecho "Configure power rules"
echo "SuspendState=freeze" >> /etc/systemd/sleep.conf

infecho "Configure feedbackd"
mkdir -p /usr/share/feedbackd/themes/
cp /root/installation/opensuse.json /usr/share/feedbackd/themes/

infecho "Configure /etc/profile.d"
cp /root/installation/opensuse-profile.sh /etc/profile.d/

infecho "Configure camera"
#cp /root/installation/camera/pinephone-camera-setup.service /usr/lib/systemd/system/
cp /root/installation/camera/pinephone-camera-setup.sh /usr/bin/
#systemctl enable pinephone-camera-setup
rm -rf /usr/share/applications/net.flyingpimonster.Camera.desktop
cp /root/installation/camera/net.flyingpimonster.CameraFront.desktop /usr/share/applications/
cp /root/installation/camera/net.flyingpimonster.CameraBack.desktop /usr/share/applications/


infecho "Clean root workspace"
echo "" > /root/.bash_history
