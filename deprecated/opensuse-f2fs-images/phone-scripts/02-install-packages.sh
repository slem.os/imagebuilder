#!/bin/bash
set -e

echo "==================="
echo "02-install-packages.sh"
echo "==================="

# Functions
infecho () {
    echo "[Info] $1"
}
errecho () {
    echo "[Error] $1" 1>&2
    exit 1
}

infecho "Setting DNS"
echo "nameserver 9.9.9.9" > /etc/resolv.conf

infecho "Setting Pinephone repository"
cp /root/installation/pinephone.repo /etc/zypp/repos.d/

infecho "Installing recommended packages..."
zypper --no-gpg-checks install --no-recommends -y \
    rtl8723cs-firmware f2fs-tools arphic-ukai-fonts ofono atinout epiphany\
    libgnome-desktop-3-19 feedbackd gnome-session gnome-themes-extras gnome-icon-theme \
    gnome-themes-lang paper-icon-theme phosh phoc iio-sensor-proxy \
    xorg-x11-server-wayland pinephone-helpers gnome-icon-theme-extras \
    gnome-icon-theme-symbolic libnotify-tools chatty purple-mm-sms v4l-utils || true

zypper install -y NetworkManager gnome-keyring || true

infecho "Installing Mobile Apps"
zypper --no-gpg-checks install --no-recommends -y calls kgx squeekboard-1.9.0 || true

infecho "Force install Mobile Gnome Packages"
zypper install --no-recommends --oldpackage -y gnome-control-center-3.36.2.pinephone \
    gnome-contacts-3.36.1.pinephone gnome-clocks-3.32.0.pinephone gtherm-0.0.2 gnome-usage-3.32.0.pinephone \
    ModemManager-1.14.0.pinephone pulseaudio-13.0.5.pinephone wys-0.1.7 pulseaudio-utils-13.0.5.pinephone \
    libaperture-0-0 pinhole-0.0.0.pinephone

infecho "Enable NetworkManager service"
systemctl disable wicked || true
systemctl enable NetworkManager || true
systemctl disable NetworkManager-wait-online.service

infecho "Enable Ofono service"
systemctl enable ofono || true

infecho "Enable Phosh service"
cp /root/installation/phosh.service /usr/lib/systemd/system/phosh.service
systemctl enable phosh

infecho "Locking kernel updates"
zypper al "kernel*"


#infecho "Upgrading packages..."
#dnf update --exclude="kernel kernel-core kernel-modules"
